const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
module.exports = {
    entry: {
        app: './src/app-src.js',

    },


    output: {
        filename: '[name].js',
        path: __dirname + '/dist/js',
        //library: 'Main'

    },
    plugins: [
        new BrowserSyncPlugin({
            // browse to http://localhost:3000/ during development,
            // ./public directory is being served
            host: 'localhost',
            port: 3010,
            server: { baseDir: ['dist'] }
        })
    ]

};