var async = require('async');
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var path = require('path');
var config = require(__dirname + '/config.js');

app.use(express.static(__dirname + '/dist/'));
app.use(bodyParser.json());




function startExpress() {
    //app._rdbConn = connection;
    app.listen(config.express.port);
    console.log('Listening on port ' + config.express.port);
}

startExpress();